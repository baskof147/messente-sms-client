<?php

namespace unit;

use baskof147\MessenteSmsClient\Provider;
use Codeception\Stub;
use Codeception\Test\Unit;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use yii\helpers\Json;

class ProviderTest extends Unit
{
    public function testAcceptStatus()
    {
        $provider = new Provider([
            'username' => 'username',
            'password' => 'password'
        ]);
        $provider->setClient(Stub::make(Client::class, [
            'send' => function () {
                return Stub::make(Response::class, [
                    'getStatusCode' => 200,
                    'getBody' => function () {
                        return Stub::make(Stream::class, [
                            'getContents' => function () {
                                return Json::encode([
                                    'messages' => [
                                        [
                                            'channel' => 'sms',
                                            'message_id' => 'fr593ce7-68de-5e44-bc50-044a3ad0a7fa',
                                            'sender' => 'Credy',
                                        ]
                                    ],
                                    'omnimessage_id' => '632c6f3d-49d0-4a8f-5k2n-74023d31e51d',
                                    'to' => '+79056542563'
                                ]);
                            }
                        ]);
                    }
                ]);
            }
        ]));

        $this->assertTrue($provider->compose('Hi!')->setTo('+79056542563')->send());
    }

    public function testRejectStatus()
    {
        $provider = new Provider([
            'username' => 'username',
            'password' => 'password'
        ]);
        $provider->setClient(Stub::make(Client::class, [
            'send' => function () {
                throw new Exception;
            }
        ]));

        $this->assertFalse($provider->compose('Hi!')->setTo('+79056542563')->send());
    }
}
