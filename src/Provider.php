<?php

namespace baskof147\MessenteSmsClient;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Messente\Api\Api\OmnimessageApi;
use Messente\Api\Configuration;
use Messente\Api\Model\Omnimessage;
use Messente\Api\Model\SMS;
use mikk150\sms\BaseProvider;
use mikk150\sms\MessageInterface;
use Throwable;
use Yii;

class Provider extends BaseProvider
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @inheritDoc
     */
    public $messageClass = Message::class;

    public $timeout = 10;

    /**
     * @var ClientInterface
     */
    private $_client;

    /**
     * @param MessageInterface $message
     *
     * {@inheritdoc}
     */
    protected function sendMessage($message)
    {
        $config = Configuration::getDefaultConfiguration()
            ->setUsername($this->username)
            ->setPassword($this->password);
        $apiInstance = new OmnimessageApi(
            $this->getClient(),
            $config
        );

        $messagesSent = true;
        foreach ((array) $message->getTo() as $recipient) {
            try {
                $omniMessage = new Omnimessage([
                    'to' => $recipient,
                ]);
                $sms = new SMS([
                    'text' => $message->getBody(),
                    'sender' => $message->getFrom(),
                ]);
                $omniMessage->setMessages([$sms]);
                $apiInstance->sendOmnimessage($omniMessage);
            } catch (Throwable $exception) {
                Yii::error('Error response: "' . $exception->getMessage() . '". Message: ' . $message->toString(), 'number');
                $messagesSent = false;
            }
        }
        return $messagesSent;
    }

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->_client = $client;
    }

    /**
     * @return ClientInterface
     */
    public function getClient()
    {
        if ($this->_client === null) {
            $this->_client = new Client([
                'timeout' => $this->timeout,
            ]);
        }
        return $this->_client;
    }
}
